 <?php 
$page_name = "Rechnungsbegleich";
include ('./header.php'); ?>


<form  method="post" action="./PDFCreators/pdfcreate_rechnung.php" accept-charset="utf-8">
	<div class="form-group">
	  <label for="buchungsnummer">Buchungsnummer</label>
	  <input name="number" type="number" class="form-control" id="buchungsnummer"  value="0" min="1">
	</div>
	<div class="form-group">
	  <label for="date">Datum der Überweisung</label>
	  <input name="date" type="date" class="form-control" id="date">
	</div>
	<div class="form-group">
	  <label for="who">zu bezahlende Firma/Gremium</label>
	  <input name="who" class="form-control" id="who" placeholder="Fachschaftsrat Chemie">
	</div>
	<div class="form-group">
	  <label for="why">Wofür?</label>
	  <input name="why" class="form-control" id="why" placeholder="T-Shirts O-Woche 2020 [(RNR 2019-01)]">
	</div>
 	<div class="form-group">
	  <label for="amount">Geldmenge</label>
	  <input name="amount" class="form-control" id="amount"  placeholder="12,00">
	</div>
	<div class="form-group">
	  <label for="reso">Nach Beschluss vom ... / Finanzerbeschluss</label>
	  <input name="reso" class="form-control" id="reso" value="Nach Beschluss vom ">
	</div>
	<button type="submit" class="btn btn-info  btn-block">Let's create!</button>
</form>


<script type="text/javascript">
	classes = document.getElementById("nav-rechnung");
	classes.classList.add("active");
	classes.classList.remove("text-secondary");
	classes.classList.add("bg-secondary");
	classes.classList.add("text-white");
</script>
<?php readfile('./foot.html'); ?>
