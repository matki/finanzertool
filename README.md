# Finanzertool

Finanzertool zur Erstellung von Buchungsbelegen. Nach Vorbild der StuRa Belege.

Unter der Leervorläge können mit \n im Text Zeilenumbrüche eingefügt werden.

Bei Kontoauszügen darauf achten, dass das Überweisungsdatum im drauf folgenden Monat geschah, bei allen anderen (zum Beispiel Abrechnung Dezember am 31.12.) bitte Sonstiges nutzen.

# Zukunft
Wenn sich Gedanken um Datensicherheit und Einbindungsmöglichkeiten gemacht wurden, ist der Plan, eine Datenbank im Hintergrund laufen zu lassen, mit welcher dann auch die Erstellung der Semester-Ein-und-Ausgangstabelle vom Tool erledigt werden kann. Damit könnte dann auch ein einfacher Überblick zu allen Ein- und Ausgängen einer Veranstaltung gegeben werden.