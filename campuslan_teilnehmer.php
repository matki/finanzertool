 <?php 
$page_name = "CampusLan Teilnehmerbeiträge";
include ('./header.php'); ?>


<form  method="post" action="./PDFCreators/pdfcreate_campuslan_teilnehmer.php" accept-charset="utf-8">
	<div class="form-group">
		  <label for="buchungsnummer">Buchungsnummer</label>
		  <input name="number" type="number" class="form-control" id="buchungsnummer"  value="0" min="1">
	</div>
	<div class="form-group">
	  	<label for="date">Datum der Überweisung</label>
	  	<input name="date" type="date" class="form-control" id="date" value="2019-10-15">
	</div>
 	<div class="form-group">
	  	<label for="amount">Geldmenge [glatte Euro]</label>
	  	<input name="amount" class="form-control" id="amount"  placeholder="15">
	</div>
	<div class="form-group">
		<label for="breakdown">Aufschlüsselung [Name: 15; Name: 20]</label>
		<input name="breakdown" class="form-control" id="breakdown" placeholder="Name Mittelname Langernachname Nochetwas: 15; kurzer Name: 20">
	</div>
	<button type="submit" class="btn btn-info  btn-block">Let's create!</button>
</form>


<script type="text/javascript">
	classes = document.getElementById("nav-sonstiges");
	classes.classList.add("active");
	classes.classList.remove("text-secondary");
	classes.classList.add("bg-secondary");
	classes.classList.add("text-white");
</script>
<?php readfile('./foot.html'); ?>
