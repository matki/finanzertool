 <?php 
$page_name = "Einnahme Defizitbezuschussung";
include ('./header.php'); ?>

<form  method="post" action="./PDFCreators/pdfcreate_ein_zuschuss.php" accept-charset="utf-8">
	<div class="form-group">
	  <label for="buchungsnummer">Buchungsnummer</label>
	  <input name="number" type="number" class="form-control" id="buchungsnummer"  value="0" min="1">
	</div>
	<div class="form-group">
	  <label for="date">Datum der Überweisung</label>
	  <input name="date" type="date" class="form-control" id="date">
	</div>
 	<div class="form-group">
	  <label for="amount">Geldmenge</label>
	  <input name="amount" class="form-control" id="amount"  placeholder="12,00">
	</div>
	<div class="form-group">
		<label for="type">Art des Geldflusses</label>
		<select name="type" class="form-control" id="type">
		  	<option selected value="Bank">Bank/Konto</option>
		  	<option value="Bar">Bar/Kasse</option>
		</select>
	</div>
	<div class="form-group">
	    <label for="why">Wofür?</label>
	    <input name="why" class="form-control" id="why" value="Informatiker-Chemiker-Event (08/2020)">
	</div>
	<div class="form-group">
	    <label for="who">Von wem?</label>
	    <input name="who" class="form-control" id="who" value="FSR Chemie">
	</div>
	<button type="submit" class="btn btn-info  btn-block">Let's create!</button>
</form>


<script type="text/javascript">
	classes = document.getElementById("nav-sonstiges");
	classes.classList.add("active");
	classes.classList.remove("text-secondary");
	classes.classList.add("bg-secondary");
	classes.classList.add("text-white");
</script>
<?php readfile('./foot.html'); ?>
