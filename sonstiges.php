 <?php 
$page_name = "Sonstiges - Leervorlage";
include ('./header.php'); ?>


<form  method="post" action="./PDFCreators/pdfcreate_sonstiges.php" accept-charset="utf-8">
	<div class="form-group">
	  <label for="buchungsnummer">Buchungsnummer</label>
	  <input name="number" type="number" class="form-control" id="buchungsnummer"  value="0" min="1">
	</div>
	<div class="form-group">
	  <label for="date">Datum der Überweisung</label>
	  <input name="date" type="date" class="form-control" id="date">
	</div>
	<div class="form-group">
	  <label for="type">Art des Geldflusses</label>
	  <select name="type" class="form-control" id="type">
	  	<option selected value="Bank, ">Einnahme Bank/Konto</option>
	  	<option value="Bar, ">Einnahme Bar/Kasse</option>
	  	<option value=", Bank">Ausgabe Bank/Konto</option>
	  	<option value=", Bar">Ausgabe Bar/Kasse</option>
	  </select>
	</div>
 	<div class="form-group">
	  <label for="amount">Geldmenge</label>
	  <input name="amount" class="form-control" id="amount"  placeholder="12,00">
	</div>
	<div class="form-group">
	  <label for="why">Wofür?</label>
	  <input name="why" class="form-control" id="why" value="">
	</div>
	<div class="form-group">
	  <label for="reso">Nach Beschluss vom ... / Finanzerbeschluss</label>
	  <input name="reso" class="form-control" id="reso" value="">
	</div>
	<div class="form-group">
		<label for="breakdown">Aufschlüsselung</label>
		<input name="breakdown" class="form-control" id="breakdown" placeholder="Beleg 1 (Edeka): 24,95; Abzüglich Pfand: -1,95; Beleg 2 (Penny): 13,44">
	</div>
	<button type="submit" class="btn btn-info  btn-block">Let's create!</button>
</form>

<script type="text/javascript">
	classes = document.getElementById("nav-sonstiges");
	classes.classList.add("active");
	classes.classList.remove("text-secondary");
	classes.classList.add("bg-secondary");
	classes.classList.add("text-white");
	//classes.classList.remove("btn-outline-secondary");
	//classes.classList.add("btn-secondary");
</script>
<?php readfile('./foot.html'); ?>
