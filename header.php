<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title><?=$page_name?></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>
	<div class="container my-5">
		<h1><a href="index.php" class="text-dark">Buchungsbeleg Creator</a></h1>	
	</div>
	<nav>
		<ul class="nav nav-tabs" id="nav-tab">  <!-- nav-pills/nav-tab; nav-fill? -->
			<li class="nav-item dropdown">
				<a class="text-secondary nav-link dropdown-toggle" data-toggle="dropdown" id="nav-kontofg" href="#" role="button" aria-haspopup="true" aria-expanded="false">Kontoführungsgebühren</a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="./kontofg.php">Normal Abschluss</a>
					<a class="dropdown-item" href="./negativzins.php">Negativzins</a>
				</div>
			</li>
			<li class="nav-item">
				<a class="text-secondary nav-link" href="./auslagen.php" id="nav-auslagen">Auslagenerstattung</a>
			</li>	
			<li class="nav-item">
				<a class="text-secondary nav-link" href="./rechnung.php" id="nav-rechnung">Rechnungsbegleich</a>
			</li>	
			<li class="nav-item">
				<a class="text-secondary nav-link" href="./campuslan_teilnehmer.php">Campuslan Teilnehmerbeitrag</a>
			</li>
			<li class="nav-item dropdown">
				<a class="text-secondary nav-link dropdown-toggle" data-toggle="dropdown" id="nav-sonstiges" href="#" role="button" aria-haspopup="true" aria-expanded="false">Sonstiges</a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="./aufwandses.php">Aufwandsentschädigung</a>
					<a class="dropdown-item" href="ein_zuschuss.php">Bezuschussung Veranstaltungen</a>
					<a class="dropdown-item" href="./fsmittel.php">Fachschaftsmittel</a>
					<a class="dropdown-item" href="./erst-kontofg.php">Erstattung Kontoführung</a>
					<a class="dropdown-item" href="zuschuss.php">Zuschuss StuRa</a>
					<a class="dropdown-item" href="umbuchung.php">Umbuchung</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="./sonstiges.php">Leervorlage</a>
				</div>
			</li>
		</ul>
	</nav>
	<div class="container mb-3 mt-3 ">
		<h1><?=$page_name?></h1>	
	</div>
	<div class="container mb-5">
		
