<?php
require('./BasicBeleg.php');

$reso = $_POST['reso'];
$why = $_POST['why'];
$who = $_POST['who'];
$type = $_POST['type'];
$list = explode(";", utf8_decode($_POST['breakdown']));

#(int $number, string $date, string $inType, string $outType, string $amount)
$pdf = new BasicBeleg($_POST['number'], date("d.m.Y", strtotime($_POST['date'])), "", $type, $_POST['amount']);

    $pdf->MultiCell(0,10, utf8_decode('Auslagenerstattung '.$why), 0,1);
    $pdf->Cell(0,10, utf8_decode('an '.$who), 0,1);
    $pdf->Cell(0,10, utf8_decode($reso), 0,1);
    $pdf->Ln(10);
    if ($type == "Bar") {
    	$pdf->Cell(0,10, utf8_decode("Unterschrift des/der Begünstigten: "), 0,1);
    	$pdf->Ln(20);
    }
    $pdf->Ln(5);
    #Aufschlüsselung
    if ($_POST['breakdown']!=""){

    	$pdf->SetHeading();
    	$pdf->SetX(25);
    	$pdf->Cell(20,0,utf8_decode('Aufschlüsselung:'),0,1);
    	$pdf->Ln(8);
    	$pdf->SetText();

    	$sum = 0;
	    $pdf->SetLeftMargin(30);
	    foreach ($list as $entry) {	
			$values = explode(":", $entry);
			$pdf->Cell(120,10, trim($values[0]), 0,0,'L');
			$pdf->Cell(0,10, trim($values[1]).EUR, 0,1,'R');
			$sum+=floatval(str_replace(",",".",trim($values[1])));
		}
		$pdf->Cell(120, 10, "Summe:", T,0);
		$pdf->Cell(0,10, number_format($sum, 2, ",","").EUR, T,1,'R');
    }
    


$pdf->Output('D', 'Beleg-'.$pdf->number.'.pdf', true);
?>
