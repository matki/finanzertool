<?php
require('./BasicBeleg.php');

$pdf = new BasicBeleg(100, date("d.m.Y", mktime(0,0,0,3,22,2012)), "Bank", "", "50,99", "", "", "");
	$pdf->SetMargins(25,15,25);
	$pdf->SetText();
	$pdf->SetX(25);
	$pdf->Cell(0,10, utf8_decode('Aufwandsentschädigung'), 0,1);
	$pdf->Cell(0,10, utf8_decode('für '), 0,1);
	$pdf->Ln(40);
	$pdf->Cell(0,10, 'Betrag: '.$pdf->amount.EUR, 0,1);
	$pdf->Cell(0,10, utf8_decode('nach Beschluss vom '), 0,1);
	$pdf->Cell(0,10, utf8_decode('Betrag wird ausgezahlt an: '.$pdf->who), 0,1);
	$pdf->Cell(0,10, utf8_decode('Unterschrift des/der Begünstigten: '), 0,1);
	$pdf->Ln(20);
	$pdf->Cell(0,10, utf8_decode('Die Finanzer weisen auf die steuerlichen Verpflichtungen hin. (EStG)'), 0,1);

$pdf->Output('D', 'Beleg-'.$pdf->number.'.pdf', true);
?>