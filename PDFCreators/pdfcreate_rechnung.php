<?php
require('./BasicBeleg.php');

$reso = $_POST['reso'];
$why = $_POST['why'];
$who = $_POST['who'];

#(int $number, string $date, string $inType, string $outType, string $amount)
$pdf = new BasicBeleg($_POST['number'], date("d.m.Y", strtotime($_POST['date'])), "", "Bank", $_POST['amount']);

    $pdf->Cell(0,10, utf8_decode('Rechnungsbegleich '.$why), 0,1);
    $pdf->Cell(0,10, utf8_decode('Zahlung an: '.$who), 0,1);
    $pdf->Cell(0,10, utf8_decode($reso), 0,1);
    $pdf->Ln(10);
    #Aufschlüsselung


$pdf->Output('D', 'Beleg-'.$pdf->number.'.pdf', true);
?>
