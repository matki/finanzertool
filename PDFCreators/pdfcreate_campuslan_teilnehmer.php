<?php
require('./BasicBeleg.php');

$list = explode(";", utf8_decode($_POST['breakdown']));

#(int $number, string $date, string $inType, string $outType, string $amount)
$pdf = new BasicBeleg($_POST['number'], date("d.m.Y", strtotime($_POST['date'])), "Bank", "", $_POST['amount'].",00");

    $pdf->MultiCell(0,10, utf8_decode('Teilnehmerbeitr' . (count($list) > 1 ? 'äge' : 'ag') . ' für CampusLan (23.11.2019), überwiesen von:'), 0);
    $pdf->Ln(5);
    #Aufschlüsselung
    if ($_POST['breakdown']!=""){
    	$sum = 0;
	    $pdf->SetLeftMargin(30);
	    foreach ($list as $entry) {	
			$values = explode(":", $entry);
			$pdf->Cell(120,10, trim($values[0]), 0,0,'L');
			$pdf->Cell(0,10, trim($values[1]).",00".EUR, 0,1,'R');
			$sum+=floatval(str_replace(",",".",trim($values[1])));
		}
		if (count($list) > 1){
			$pdf->Cell(120, 10, "Summe:", T,0);
			$pdf->Cell(0,10, number_format($sum, 2, ",","").EUR, T,1,'R');
		}
    }
    


$pdf->Output('D', 'Beleg-'.$pdf->number.'.pdf', true);
?>
