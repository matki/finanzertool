<?php
require('./BasicBeleg.php');

$reso = $_POST['reso'];
$why = $_POST['why'];
$who = $_POST['who'];

#(int $number, string $date, string $inType, string $outType, string $amount)
$pdf = new BasicBeleg($_POST['number'], date("d.m.Y", strtotime($_POST['date'])), "", $_POST['type'], $_POST['amount']);
    
    $pdf->Cell(0,10, utf8_decode('Aufwandsentschädigung'), 0,1);
    $pdf->Cell(0,10, utf8_decode($why), 0,1);
    $pdf->Ln(10);
    $pdf->Cell(0,10, 'Betrag: '.$pdf->amount.EUR, 0,1);
    $pdf->Cell(0,10, utf8_decode("Nach Beschluss vom ".$reso), 0,1);
    $pdf->Cell(0,10, utf8_decode('Betrag wird ausgezahlt an: '.$who), 0,1);
    $pdf->Cell(0,10, utf8_decode('Unterschrift des/der Begünstigten: '), 0,1);
    $pdf->Ln(20);
    $pdf->Cell(0,10, utf8_decode('Die Finanzer weisen auf die steuerlichen Verpflichtungen hin. (EStG)'), 0,1);


$pdf->Output('D', 'Beleg-'.$pdf->number.'.pdf', true);
?>






















