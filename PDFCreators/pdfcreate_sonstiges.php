<?php
require('./BasicBeleg.php');

$types=explode(', ', $_POST['type']);
$text=explode('\n', $_POST['why']);
$list = explode(";", utf8_decode($_POST['breakdown']));

#(int $number, string $date, string $inType, string $outType, string $amount)
$pdf = new BasicBeleg($_POST['number'], date("d.m.Y", strtotime($_POST['date'])), $types[0], $types[1] , $_POST['amount']);
	
	foreach ($text as $part) {
   		$pdf->MultiCell(0,10, utf8_decode(trim($part)), 0,1);
	}
    $pdf->MultiCell(0,10, utf8_decode($_POST['reso']), 0,1);
    $pdf->Ln(10);
    #Unterschriften
    if ($types[0] == "Bar") {
    	$pdf->Cell(0,10, utf8_decode("Betrag bar eingenommen: "), 0,1);
    	$pdf->Ln(20);
    }
    if ($types[1] == "Bar") {
    	$pdf->Cell(0,10, utf8_decode("Unterschrift des/der Begünstigten: "), 0,1);
    	$pdf->Ln(20);
    }
    #Aufschlüsselung
    if ($_POST['breakdown']!=""){

    	$pdf->SetHeading();
    	$pdf->SetX(25);
    	$pdf->Cell(20,0,utf8_decode('Aufschlüsselung:'),0,1);
    	$pdf->Ln(8);
    	$pdf->SetText();

    	$sum = 0;
	    $pdf->SetLeftMargin(30);
	    foreach ($list as $entry) {	
			$values = explode(":", $entry);
			$pdf->Cell(120,10, trim($values[0]), 0,0,'L');
			$pdf->Cell(0,10, trim($values[1]).EUR, 0,1,'R');
			$sum+=floatval(str_replace(",",".",trim($values[1])));
		}
		$pdf->Cell(120, 10, "Summe:", T,0);
		$pdf->Cell(0,10, number_format($sum, 2, ",","").EUR, T,1,'R');
    }
   
$pdf->Output('D', 'Beleg-'.$pdf->number.'.pdf', true);
?>






















