<?php
require('./fpdf.php');
define('EUR',chr(128));

date_default_timezone_set("Europe/Berlin");

class BasicBeleg extends FPDF{
	public $number, $inType, $outType, $date, $amount;

	public function __construct(int $number, string $date, string $inType, string $outType, string $amount){
			$this->number 	 = $number;
			$this->date   	 = $date;
			$this->inType 	 = $inType;
			$this->outType	 = $outType;
			$this->amount 	 = $amount;

			parent::__construct();	
			$this->SetLeftMargin(25);
			$this->SetRightMargin(15);
			$this->SetTopMargin(15);
			$this->AddPage();	
	}

	public function Header(){
		$beleg ='Buchungsbeleg';
		$this->SetFont('Arial','B',35);
		$w = $this->GetStringWidth($beleg)+6;
		$this->Cell($w,10,$beleg);
		$this->SetHeading();
		$this->Cell(20);
		$this->Cell(15,10,'Nummer:');
    	$this->Cell(20);
    	$this->Cell(15,10,$this->number,1,0,'C');
    	$this->Ln(25);
 		$this->Cell(45,16,'Datum',1,0,'C');
 		$this->Cell(60,10,'Ausgang', 'LTR',0,'C');
 		$this->Cell(60,10,'Eingang', 'LTR',1,'C'); 
		$this->Cell(45);
		$this->SetSmallText();
		for($i=0; $i<2; $i++){
			$this->Cell(20,6,'Konto', 'LB',0 ,'C'); 
			$this->Cell(40,6,'Betrag', 'RB',0 ,'C'); 
		}
		$this->Ln(6);
		$this->SetHeading();
 		$this->Cell(45,10,$this->date,1,0,'L');
		$this->Cell(20,10,$this->outType, 1,0 ,'C'); 
		if ($this->outType!="") {
			$this->Cell(40,10,$this->amount.EUR, 1,0 ,'C'); 
		} else {
			$this->Cell(40,10,'', 1,0 ,'C'); 
		} 
		$this->Cell(20,10,$this->inType, 1,0 ,'C'); 
		if ($this->inType!="") {
			$this->Cell(40,10,$this->amount.EUR, 1,0 ,'C'); 
		} else {
			$this->Cell(40,10,'', 1,0 ,'C'); 
		}
		$this->Ln(25);
		$this->SetHeading();
		$this->Cell(20,0,'Grund:',0,1);
		$this->Ln(10);
		$this->SetMargins(25,15,25);
    	$this->SetText();
    	#$this->SetX(25);
	}

	public function Footer(){
		$this->SetY(-55);
    	$this->SetHeading();
    	$this->SetLeftMargin(25);
    	$this->SetX(25);
    	$this->Cell(20,0,'Unterschriften:',0,1);
   		$this->Ln(30);
    	$this->SetSmallText();
    	$this->Cell(30);
    	$this->Cell(50,10,'sachlich richtig', 'T', 0, 'C');
		$this->Cell(20);
    	$this->Cell(50,10,'rechnerisch richtig', 'T', 1, 'C');
	}

	function SetText(){
	 	$this->SetFont('Arial', '', 14);
	}

	function SetSmallText(){
		$this->SetFont('Arial', '', 10);
	}

	function SetHeading(){
	 	$this->SetFont('Arial', '', 22);
	}

	

}


?>
